$(".video").on("click",".inner",function(){
    var $this = $(this);
    var video = $this.attr("data-iframe");
    layer.open({
      type: 2,
      title: false,
      closeBtn: 1, //不显示关闭按钮
      shade: 0.3,
      skin: 'layer-alert-video',
      area: ['900px', '563px'],
      anim: 0,
      content: [video, 'no'] //iframe的url，no代表不显示滚动条
    });
});