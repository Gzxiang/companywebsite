jQuery(".large-switch").slide({
    autoPlay: false,
    trigger: "click",
    delayTime: 1000
});


//tab切换		
$('.tab-button').click(function() {
    var tab = $(this).data('tab')
    $(this).addClass('cur').siblings('.tab-button').removeClass('cur');
    $('#tab-' + tab + '').addClass('active').siblings('.tab-item').removeClass('active');
});
//新闻列表切换
$('.information-tab .article-list').hover(function() {
    $(this).addClass('current').siblings('.article-list').removeClass('current');
}, function() {
    $(this).parent('.information-right').find('.article-list:first-of-type').addClass('current').siblings('.article-list').removeClass('current');
});