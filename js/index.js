  // 轮播图
var mySwiper = new Swiper ('.swiper-container', {
    direction: 'horizontal', // 垂直切换选项
    loop: true, // 循环模式选项
    autoplay:true,
    // 如果需要分页器
    pagination: {
      el: '.swiper-pagination',
    },
    
    // 如果需要前进后退按钮
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    
    // 如果需要滚动条
    scrollbar: {
      el: '.swiper-scrollbar',
    },
  })    
  // 选项卡
  $(".mytab li").click(function () {
    $(this).addClass("active1").siblings().removeClass("active1");
   let $index=$(this).index();
   $(".mytab_nav li").eq($index).show().siblings().hide()
})   